---
--- @File     : main
--- @Time     : 2023-09-11 22:30:43
--- @Docs     : 主程序入口
---
require "import"
import "android.app.*"
import "android.os.*"
import 'android.view.*'
-- 窗口
import 'android.widget.*'
import 'android.widget.LinearLayout'
import 'android.widget.ImageView'
import 'android.widget.Space'
-- 基础类库
import "utils.easy"

import "views.LoginActivity"

--[[
    - 导入基础类库
    - 设置程序主页面
]]
LoadImportBase()
LoadIndexPage(LoginLayout)


-- Update = function(txt)
--     show.Text = txt
-- end


-- MassCode = {
--     StreamOutput = function(option)
--         require "import"
--         UpNotify    = import "utils.loadStyle.dailog"
--         local str   = " " .. option.data .. "  "
--         local slg   = utf8.len(str)
--         local speed = option.speed or 40

--         local sgg   = function(s, i, j)
--             i, j = tonumber(i), tonumber(j)
--             i = utf8.offset(s, i)
--             j = ((j or -1) == -1) or utf8.offset(s, j + 1) - 1
--             return string.sub(s, i, j)
--         end

--         for i = 1, slg do
--             if i + 2 <= slg then
--                 call("Update", sgg(str, 1, i) .. "❤")
--                 Thread.sleep(speed)
--             end
--         end
--     end
-- }

-- local input =
-- "(╯°□°）╯︵ ┻━┻ ( ꒪Д꒪)ノ    ∑(O_O；) \n 自由 平等 公正 法治 爱国 敬业 诚信 友善 \n以爱国主义为核心，团结统一，爱好和平，勤劳勇敢，自强不息的伟大民族精神---中华民族精神内涵"


-- thread(MassCode.StreamOutput, {
--     speed = 30,
--     data = input
-- })

-- 更新 = function(str)
--     show.Text = str
-- end

-- 线程 = function(str)
--     require "import"
--     str = " " .. str .. "  "

--     -- 字符串长度只需计算一次
--     local slg = utf8.len(str)

--     -- 定义截取子字符串的函数
--     local sgg = function(s, i, j)
--         i, j = tonumber(i), tonumber(j)
--         i = utf8.offset(s, i)
--         j = ((j or -1) == -1) or utf8.offset(s, j + 1) - 1
--         return string.sub(s, i, j)
--     end

--     for i = 1, slg do
--         if i + 2 <= slg then
--             call("更新", sgg(str, 1, i) .. "❤")
--             Thread.sleep(40)
--         end
--     end
--     print("输出完成")
-- end
