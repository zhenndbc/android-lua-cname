---
--- @File     : loadCname
--- @Time     : 2023-09-05 21:48:26
--- @Docs     : 课表数据载入
require "import"
import "utils.easy"
import "utils.tool"
import "java.io.File"
local kox = import "knet.kox"

local CnameCaller = {
    BaseURL = "http://1.117.154.114:5900/api/v1/classTable",
    Spider = kox
}

function CnameCaller:SetParams(cname, week)
    self.Params = "cname=" .. StringToUnicode(cname) .. "&week=" .. week .. "&weekly="
    self.SuccessCaller = function(code, content)
        if code == 200 then
            local caller = function(err)
                print("[DEBUG] " .. err)
            end

            local filename = SaveStoragePathLocal("res", cname .. week .. "课表" .. ".png")

            return xpcall(function()
                -- 智能调用(存储图片)
                tool.WriteToFile(filename, content)
                UpNotify("图片存储成功: " .. filename)
            end, caller)
        end
        UpNotify("课表数据获取失败")
    end
end

-- function CnameCaller:SetViewPage(viewID, filename)
--     if filename == nil or "" then
--         print("这是空的")
--     end
--     local bitmap = GetImageDrawable(filename)
--     viewID.setImageBitmap(bitmap)
-- end

function CnameCaller:Start()
    local Request = self.Spider
    Request:setUrl(CnameCaller.BaseURL)
    Request:setMethod("Post")
    Request:setData(self.Params)
    Request:setHeaders("Content-Type", "application/x-www-form-urlencoded")
    Request:send(self.SuccessCaller, kox.Err)
end

return CnameCaller

--[[ -- 调用示例
CnameCaller:SetParams("2022级学前教育12班", "3")
CnameCaller:Start() ]]
