-- 导入必要的类和库
require "import"
import "utils.easy"
LoadImportBase()


-- 创建一个自定义的 EditText 样式
function CreateEditTextStyle()
    local shape = GradientDrawable()
    shape.setShape(GradientDrawable.RECTANGLE)
    shape.setCornerRadius(75)
    shape.setColor(0xffffffff)

    local focusedShape = GradientDrawable()
    focusedShape.setShape(GradientDrawable.RECTANGLE)
    focusedShape.setCornerRadius(75)
    focusedShape.setColor(0xffffffff)
    focusedShape.setStroke(8, 0xff000000)

    local states = StateListDrawable()
    states.addState({ android.R.attr.state_focused }, focusedShape)
    states.addState({}, shape)

    return states
end

-- 创建登录页面布局
LoginLayout = {
    LinearLayout, -- 线性布局
    orientation = 'vertical',
    layout_width = 'fill',
    layout_height = 'fill',
    background = ColorDrawable(0xff77AF9C),
    gravity = 'center',
    {
        ImageView,
        layout_width = '100dp',
        layout_height = '100dp',
        src = '#drawable/ic_launcher',
        layout_marginBottom = '16dp',
    },
    {
        TextView,
        id = 'titleLabel',
        layout_width = 'wrap',
        layout_height = 'wrap',
        text = '登录',
        textSize = '24sp',
        textColor = '#ffffff',
        layout_marginBottom = '32dp',
        Typeface = Typeface.DEFAULT_BOLD,
    },
    {
        EditText,
        id = 'usernameEditText',
        layout_width = '80%w',
        layout_height = 'wrap',
        hint = '用户名',
        textSize = '16sp',
        textColor = '#000000',
        layout_marginBottom = '16dp',
        background = CreateEditTextStyle(),
        inputType = InputType.TYPE_CLASS_TEXT,
    },
    {
        EditText,
        id = 'passwordEditText',
        layout_width = '80%w',
        layout_height = 'wrap',
        hint = '密码',
        textSize = '16sp',
        textColor = '#000000',
        layout_marginBottom = '32dp',
        background = CreateEditTextStyle(),
        inputType = InputType.TYPE_CLASS_TEXT + InputType.TYPE_TEXT_VARIATION_PASSWORD,
    },
    {
        LinearLayout,
        layout_width = '80%w',
        layout_height = 'wrap',
        layout_gravity = 'center_horizontal',
        gravity = 'center_horizontal',
        {
            Button,
            text = '登录',
            textSize = '16sp',
            textColor = '#ffffff',
            layout_width = '40%w',
            layout_height = '150',
            Background = LuaDrawable(function(canvas, paint, draw)
                paint.setColor(0xff000000)
                paint.setStyle(Paint.Style.STROKE)
                paint.setStrokeWidth(8)
                canvas.drawRoundRect(RectF(draw.getBounds()), 75, 75, paint)
            end),
            onClick = function()
                -- 获取用户名和密码输入框的内容
                local username = usernameEditText.getText().toString()
                local password = passwordEditText.getText().toString()

                if username == "admin" and password == "admin" then
                    -- 线程加载
                    SkipPage("views.play_animation")
                    activity.finish()
                else
                    UpNotify("密码错误啦!")
                end
            end,
        },
    },
}
