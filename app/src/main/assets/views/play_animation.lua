-- 导入必要的类和库
require "import"
import "utils.easy"
import "android.os.Handler"
LoadImportBase() -- 导入基础模块


local playAnimationLayout = {
    FrameLayout,
    layout_height = "fill",
    layout_width = "fill",
    BackgroundColor = '#00000000', -- 按钮背景颜色,
    {
        TextView,
        layout_height = "20%w",
        layout_width = "20%w",
        gravity = "center",
        layout_gravity = "center",
        id = "mof_lay"
    },
};
-- 载入视图
activity.setContentView(loadlayout(playAnimationLayout))
mof_lay.setBackgroundDrawable(DrawRubik())
-- 定时器
local handler = Handler()
handler.postDelayed(function()
    -- SkipPage("views.indexPage")
    SkipPage("core.Robot.ChatBot")
    activity.finish()
end, 3000)
