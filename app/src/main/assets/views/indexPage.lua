require "import"
import "utils.easy"
LoadImportBase() -- 导入基础模块

-- 主界面
indexLay = {
    LinearLayout,
    layout_width = "fill",
    orientation = "vertical",
    layout_height = "fill",
    {
        CardView,
        layout_gravity = "center",
        layout_width = "fill",
        layout_height = "40%w",
        id = "card_outside",
        layout_margin = "15dp",
        elevation = "10dp",
        radius = "35dp",
        CardBackgroundColor = "#ffffffff",
        {
            ImageView,
            src = "https://pica.zhimg.com/80/v2-45fcfb467fc76d0d34f3aec712bad93a_1440w.webp?source=1940ef5c",
            id = "card_img",
            layout_width = "fill",
            layout_height = "fill",
            scaleType = "fitXY",
            layout_gravity = "left|center"
        }
    },
    {
        CardView,
        layout_gravity = "bottom|center",
        layout_width = "25%w",
        layout_height = "10%w",
        id = "card_logo",
        layout_margin = "fill",
        elevation = "3dp",
        radius = "35dp",
        CardBackgroundColor = "#ffffffff",
        {
            ImageView,
            src = "https://q1.qlogo.cn/g?b=qq&nk=123456&s=100",
            layout_width = "30dp",
            layout_height = "30dp",
            scaleType = "fitXY",
            layout_marginLeft = "10dp",
            layout_gravity = "left|center",
            id = "logo_img"
        },
        {
            TextView,
            textSize = "17sp",
            layout_width = "fill",
            layout_height = "fill",
            gravity = "right|center",
            id = "show_text",
            layout_marginRight = "25dp",
            text = "你好"
        }
    },
    {
        Button,
        layout_gravity = "center",
        id = "show_logo",
        text = "请求"
    }
};

---- 设置主页面的图片
--IndexLayImg = function(newSrc)
--
--end
--
---- 设置主页面的Logo图片
--IndexLayLogoImg = function(newSrc)
--    logo_img.src = newSrc
--end
--
---- 设置主页面按钮事件
--IndexLayButton = function(callBack)
--    show_logo.onClick = callBack
--end

--
-- 工具函数
local cjson = import "cjson"
-- local ZoomPage = import "utils.expand.img_zoom"
local CnameCaller = import "api.loadCname"
--
---- 载入配置
-- local user_info = cjson.decode(Tool.load_file("config.json"))
-- local spider = login:new(user_info) -- 初始化爬虫
--
--local cname, week = "2022级学前教育12班", "3"
--
-- function show_logo.onClick()
--     task(100,
--         function()
--             CnameCaller:SetParams(cname, week)
--             CnameCaller:Start()
--         end
--     )
-- end

-- local info = "2022级学前教育12班3课表"
-- UpNotify(info)
-- logo_img.onTouch = function()
--     logo_img.setImageBitmap(loadbitmap(SaveStoragePathLocal("res", info .. ".png")))
-- end

--card_img.onTouch = function()
--    card_img.setImageBitmap(loadbitmap(SaveStoragePathLocal("res", "2022级学前教育12班3课表" .. ".png")))
--end
--
--skip_page.onClick = function()
--    activity.newActivity(loadlayout(ZoomPage))
--end

activity.setContentView(loadlayout(indexLay))
