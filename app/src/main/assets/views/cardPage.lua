require "import"
import "utils.easy"
LoadImportBase() -- 导入基础模块

local cardPageLayout = {
    FrameLayout,
    layout_width = "fill",
    layout_height = "fill",
    {
        CardView,
        layout_gravity = "center",
        layout_width = "match_parent",
        layout_height = "match_parent",
        elevation = "10dp",
        radius = "35dp",
        CardBackgroundColor = "#ffffffff",
        {
            TextView,
            layout_width = "wrap_content",
            layout_height = "wrap_content",
            text = "Hello, World!",
            textSize = "24sp",
            textColor = "#000000",
            gravity = "center",
        },
    },
}

activity.setContentView(loadlayout(cardPageLayout))
