---
--- @File     : tool
--- @Time     : 2023-09-05 21:52:15
--- @Docs     : 文件工具类
require "import"
import "utils.easy"
import "java.io.File"

-- 创建文件工具类
FileUtil = {}

--- 创建新文件（java）
--- @param filePath string 文件路径
function FileUtil.createNewFile(filePath)
    File(filePath).createNewFile()
end

--- 创建新文件夹（java）
--- @param folderPath string 文件夹路径
function FileUtil.createNewFolder(folderPath)
    File(folderPath).mkdir()
end

--- 重命名与移动文件（lua）
--- @param oldPath string 原文件路径
--- @param newPath string 新文件路径
function FileUtil.renameAndMoveFile(oldPath, newPath)
    os.rename(oldPath, newPath)
end

--- 追加更新文件（lua）
--- @param filePath string 文件路径
--- @param content string 要追加的内容
function FileUtil.appendToFile(filePath, content)
    io.open(filePath, "a+"):write(content):close()
end

--- 更新文件（lua）
--- @param filePath string 文件路径
--- @param content string 要写入的内容
function FileUtil.updateFile(filePath, content)
    io.open(filePath, "w+"):write(content):close()
end

--- 写入文件（自动创建父文件夹）
--- @param filePath string 文件路径
--- @param content string 要写入的内容
function FileUtil.writeToFile(filePath, content, ...)
    local op = { ... }
    filePath = File(filePath)

    -- 检查父级目录是否存在，如果不存在则创建
    local parentFile = filePath.getParentFile()
    if not parentFile.exists() then
        parentFile.mkdirs()
    end

    local file = io.open(tostring(filePath), op.method or "wb")
    if file ~= nil then
        file:write(content)
        file:flush()
        file:close()
    end
end

--- 读取文件
--- @param filePath string 文件路径
function FileUtil.readFile(filePath)
    return io.open(filePath):read("*a")
end

--- 按行读取文件
--- @param filePath string 文件路径
function FileUtil.readLinesFromFile(filePath)
    local lines = {}
    for line in io.lines(filePath) do
        table.insert(lines, line)
    end
    return lines
end

--- 删除文件或文件夹
--- @param path string 文件或文件夹路径
function FileUtil.deleteFileOrFolder(path)
    local file = File(path)
    if file.isDirectory() then
        LuaUtil.rmDir(path)
    else
        file.delete()
    end
end

--- 复制文件
--- @param sourcePath string 源文件路径
--- @param destinationPath string 目标文件路径
function FileUtil.copyFile(sourcePath, destinationPath)
    LuaUtil.copyDir(File(sourcePath), File(destinationPath))
end

--- 递归删除文件夹或文件
--- @param path string 文件夹或文件路径
function FileUtil.recursivelyDelete(path)
    LuaUtil.rmDir(path)
end

--- 替换文件内字符串
--- @param filePath string 文件路径
--- @param searchString string 要查找的字符串
--- @param replacement string 替换的字符串
function FileUtil.replaceStringInFile(filePath, searchString, replacement)
    if filePath then
        filePath = tostring(filePath)
        local content = io.open(filePath):read("*a")
        io.open(filePath, "w+"):write(tostring(content:gsub(searchString, replacement))):close()
    else
        return false
    end
end

--- 获取文件列表
--- @param folderPath string 文件夹路径
function FileUtil.getFileList(folderPath)
    local file = File(folderPath)
    return luajava.astable(file.listFiles())
end

--- 获取文件名称
--- @param filePath string 文件路径
function FileUtil.getFileName(filePath)
    return File(filePath).getName()
end

--- 获取文件大小
--- @param filePath string 文件路径
function FileUtil.getFileSize(filePath)
    local size = File(filePath).length()
    return android.text.format.Formatter.formatFileSize(activity, size)
end

--- 获取文件或文件夹最后修改时间
--- @param filePath string 文件或文件夹路径
function FileUtil.getFileLastModifiedTime(filePath)
    local file = File(filePath)
    local cal = java.util.Calendar.getInstance()
    cal.setTimeInMillis(file.lastModified())
    return cal.getTime().toLocaleString()
end

--- 获取文件字节
--- @param filePath string 文件路径
function FileUtil.getFileByte(filePath)
    return File(filePath).length()
end

--- 获取文件父文件夹路径
--- @param filePath string 文件路径
function FileUtil.getFileParentFolder(filePath)
    return tostring(File(filePath).getParentFile())
end

--- 获取文件Mime类型
--- @param name string 文件名
function FileUtil.getFileMimeType(name)
    local MimeTypeMap = import "android.webkit.MimeTypeMap"
    local extensionName = tostring(name):match("%.(.+)")
    local mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extensionName)
    return tostring(mime)
end

--- 判断路径是不是文件夹
--- @param path string 路径
function FileUtil.isDirectory(path)
    return File(path).isDirectory()
end

--- 判断路径是不是文件
--- @param path string 路径
function FileUtil.isFile(path)
    return File(path).isFile()
end

--- 判断文件或文件夹存不存在
--- @param path string 路径
function FileUtil.exists(path)
    return File(path).exists()
end

--- 判断是不是系统隐藏文件
--- @param path string 路径
function FileUtil.isHidden(path)
    return File(path).isHidden()
end

--- 载入基础配置文件
--- @param filename string 文件名
function FileUtil.loadFile(filename)
    local lua_dir = activity.getLuaDir() .. "/" .. "config" .. "/"
    local file = io.open(lua_dir .. filename, "r")
    if file then
        local content = file:read("*a")
        file:close()
        return content
    end
end

return FileUtils
