---
--- @File     : easy
--- @Time     : 2023-09-05 21:51:38
--- @Docs     : 超级简单的工具合集
require "import"
import "android.os.*"
import "java.io.File"
import "android.net.Uri"
import "java.net.URLEncoder"
import "android.content.Intent"
import "android.content.Context"
import "android.graphics.drawable.GradientDrawable"

-- 获取手机存储路径
GetStoragePath = function()
    return Environment.getExternalStorageDirectory().toString()
end

-- 获取手机宽度
GetScreenWidth = function()
    return activity.getWidth()
end

-- 获取手机屏幕高度
GetScreenHight = function()
    return activity.getHeight()
end

-- 获取软件包名--本身
GetPackageName = function()
    return activity.getPackageName()
end

-- 获取当前的时间
GetNowTime = function()
    return os.date("%Y-%m-%d %H:%M:%S")
end

-- 判断文件是否存在
FileExists = function(id)
    import ""
    return File(id).exists()
end

-- 显示控件
Nconceal = function(id)
    id.setVisibility(0)
end

-- 不影响控件
Conceal = function(id)
    id.setVisibility(8)
end

-- 控件圆角
RedidView = function(view, InsideColor, radiu)
    local drawable = GradientDrawable()
    drawable.setShape(GradientDrawable.RECTANGLE)
    drawable.setColor(InsideColor)
    drawable.setCornerRadii({ radiu, radiu, radiu, radiu, radiu, radiu, radiu, radiu });
    view.setBackgroundDrawable(drawable)
end

-- 控件圆角 2
RedidViewTwo = function(view, InsideColor, radiu)
    local drawable = GradientDrawable()
    drawable.setShape(GradientDrawable.RECTANGLE)
    drawable.setColor(InsideColor)
    drawable.setCornerRadii({ radiu, radiu, 0, radiu, 0, radiu, radiu, radiu });
    view.setBackgroundDrawable(drawable)
end

-- 获取图片BitmapDrawable
GetImageDrawable = function(img_path)
    import "java.io.FileInputStream"
    import "android.graphics.BitmapFactory"
    import "android.graphics.drawable.BitmapDrawable"
    -- 引用Java的FileInputStream类
    -- local FileInputStream = luajava.bindClass "java.io.FileInputStream"
    -- -- 引用Android的BitmapFactory类
    -- local BitmapFactory = luajava.bindClass "android.graphics.BitmapFactory"
    -- -- 引用Android的BitmapDrawable类
    -- local BitmapDrawable = luajava.bindClass "android.graphics.drawable.BitmapDrawable"
    -- 打开文件输入流 读取图像文件
    local image_stream = FileInputStream(img_path)
    -- 使用BitmapFactory解码图像流 生成Bitmap对象
    local bitmap = BitmapFactory.decodeStream(image_stream)
    -- 使用Bitmap对象创建一个BitmapDrawable对象并返回
    return BitmapDrawable(activity.getResources(), bitmap)
end

-- 存储到系统路径 => SaveStoragePathLocal("res", "config.json")
SaveStoragePathLocal = function(...)
    local args = { ... }
    local saveDir = GetStoragePath()
    if #args ~= 0 then
        for _, value in ipairs(args) do
            saveDir = saveDir .. "/" .. value
        end
    end
    return saveDir
end

-- 跳转页面
SkipPage = function(viewPath)
    local skipPath = activity.getLuaDir()
    if not string.find(viewPath, "[.]") then
        -- 如果不包含点号，则不需要进行分割，直接拼接
        -- activity.newActivity( .. "/" .. viewPath .. ".lua")
        skipPath = skipPath .. "/" .. viewPath
    else
        local parts = {}
        for part in string.gmatch(viewPath, "[^%.]+") do
            table.insert(parts, part)
        end
        local newPath = table.concat(parts, "/")
        skipPath = skipPath .. "/" .. newPath
    end
    activity.newActivity(skipPath)
end

-- 载入初始化页面
LoadIndexPage = function(view)
    Activity = _G.activity
    Loadlayout = _G.loadlayout
    Activity.setContentView(loadlayout(view))
end

-- 布局内插入布局
LayAddView = function(view, lay)
    view.addView(loadlayout(lay))
end

-- 使弹出的输入法不影响布局
LayoutNotAffect = function()
    activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
end

-- 重置当前页面
ResetPage = function()
    activity.recreate()
end

-- 获取剪贴板
GetClipboard = function()
    return activity.getSystemService(Context.CLIPBOARD_SERVICE).getText()
end

-- 写入剪贴板
InputClipboard = function(text)
    activity.getSystemService(Context.CLIPBOARD_SERVICE).setText(text)
end

-- 设置屏幕旋转为自动旋转模式
SetAutoRotateMode = function()
    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR)
end

-- 设置屏幕旋转为横屏模式
SetLandscapeMode = function()
    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
end

-- 设置屏幕旋转为竖屏模式
SetPortraitMode = function()
    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
end

-- 请求悬浮窗权限
RequestOverlayPermission = function()
    local intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
    intent.setData(Uri.parse("package:" .. activity.getPackageName()))
    activity.startActivityForResult(intent, 100)
end

-- 进入指定页面
GoToPage = function(na)
    activity.newActivity(na)
end

-- 进入首页
GoToMainPage = function()
    activity.newActivity("main")
end

-- 获取当前的时间
GetSystemTime = function()
    return os.date("%Y-%m-%d %H:%M:%S")
end

-- 打开应用程序
OpenApp = function(packageName)
    local manager = activity.getPackageManager()
    local open = manager.getLaunchIntentForPackage(packageName)
    activity.startActivity(open)
end

-- 安装程序
InstallApp = function(packageName)
    local intent = Intent(Intent.ACTION_VIEW)
    intent.setDataAndType(Uri.parse("file://" .. packageName), "application/vnd.android.package-archive")
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    activity.startActivity(intent)
end

-- 卸载应用
RemoveApp = function(packageName)
    local uri = Uri.parse("package:" .. packageName)
    local intent = Intent(Intent.ACTION_DELETE, uri)
    activity.startActivity(intent)
end

-- 开启WIFI
OpenWIFI = function()
    local wifi = activity.Context.getSystemService(Context.WIFI_SERVICE)
    wifi.setWifiEnabled(true)
end

-- 关闭WIFI
CloseWIFI = function()
    import "android.content.Context"
    local wifi = activity.Context.getSystemService(Context.WIFI_SERVICE)
    wifi.setWifiEnabled(false)
end

-- 断开网络
CloseNetWork = function()
    import "android.content.Context"
    local wifi = activity.Context.getSystemService(Context.WIFI_SERVICE)
    wifi.disconnect()
end

-- 联系QQ
FollowQQ = function(qqNumber)
    local url = "mqqwpa://im/chat?chat_type=wpa&uin=" .. qqNumber
    activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
end

-- 加群
FollowGroup = function(groupNumber)
    import "android.net.Uri"
    import "android.content.Intent"
    local url = "mqqapi://card/show_pslcard?src_type=internal&version=1&uin=" .. groupNumber ..
        "&card_type=group&source=qrcode"
    activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
end

-- 删除控件
RemoveView = function(a, b)
    return (a).removeView(b)
end

-- 函数将字符串转换为Unicode编码
StringToUnicode = function(str)
    return URLEncoder.encode(str, "UTF-8")
end

-- 导入基础模块
LoadImportBase = function()
    import "android.app.*"
    import "android.os.*"
    import 'android.view.*'
    -- 窗口
    import 'android.widget.*'
    import 'android.widget.LinearLayout'
    import 'android.widget.ImageView'
    import 'android.widget.Space'
    -- 上下文
    import 'android.content.Intent'
    import "android.content.Context"
    -- 设计
    import 'android.graphics.*'
    import "android.graphics.RectF"
    import "android.graphics.Paint"
    import 'android.graphics.Typeface'
    import "android.graphics.drawable.Drawable"
    import 'android.graphics.drawable.ClipDrawable'
    import 'android.graphics.drawable.LayerDrawable'
    import "android.graphics.drawable.ShapeDrawable"
    import "android.graphics.drawable.ColorDrawable"
    import "android.graphics.drawable.GradientDrawable"
    import 'android.graphics.drawable.StateListDrawable'
    import "android.graphics.drawable.shapes.RoundRectShape"
    -- 文本输入
    import 'android.text.InputType'
    import "android.text.Spannable"
    import "android.text.SpannableString"
    import "android.text.style.ForegroundColorSpan"
    -- 自定义类库
    import "utils.logs"             -- 日志
    import "utils.loadStyle.mof"    -- 悬浮魔方
    import "utils.loadStyle.dailog" -- 提示器

    import "android.graphics.PixelFormat"
    import "android.content.Context"
    import "android.provider.Settings"
    import "android.animation.ObjectAnimator"
    import "android.animation.ArgbEvaluator"
    -- 尝试导入
    pcall(function(...)
        _G.EMUIStyle = import "EMUIStyle"
    end)
end
