require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "android.view.animation.Animation"
import "android.view.animation.RotateAnimation"

import "utils.easy"

function UpNotify(txt, ...)
    local color = 0x60000000
    local noticeLay = {
        LinearLayout,
        orientation = "vertical",
        layout_height = "fill",
        layout_width = "fill",
        {
            LinearLayout,
            layout_width = "-2",
            background = "#FF4CAF50",
            gravity = "center",
            orientation = "horizontal",
            layout_height = "43dp",
            layout_gravity = "center",
            id = "bj",
            {
                LinearLayout,
                layout_height = "match_parent",
                gravity = "center",
                layout_width = "50dp",
                background = "#ffffffff",
                id = "bj1",
                {
                    ImageView,
                    src = "http://5b0988e595225.cdn.sohucs.com/images/20180821/6ad96f40a88f41598713aaf4377fc0ec.jpeg",
                    layout_height = "40dp",
                    layout_width = "40dp",
                    id = "notify_img"
                }
            },
            {
                TextView,
                text = "",
                id = "notify_txt",
                textColor = "#FFFFFF",
                layout_gravity = "center",
                layout_marginRight = "18dp",
                layout_marginLeft = "15dp"
            }
        }
    };

    local op = { ... }
    local view = loadlayout(noticeLay)
    notify_txt.setText(txt)

    if op.src ~= nil then
        notify_img.src = op.src
    end

    -- 旋转动画
    RotationView = RotateAnimation(360, 0, Animation.RELATIVE_TO_SELF, 0.5, Animation.RELATIVE_TO_SELF, 0.5).setDuration(3000)
        .setFillAfter(true).setRepeatCount(-1)
    notify_img.startAnimation(RotationView)
    -- 控件圆角
    RedidView(bj, color, 20)
    RedidViewTwo(bj1, 0xffffffff, 20)
    -- 提示显示
    Toast.makeText(activity, "", Toast.LENGTH_LONG).setGravity(Gravity.CENTER, 0, -1000).setView(view).show() -- -1000是高度
end
