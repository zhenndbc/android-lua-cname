---
--- @File     : mof
--- @Time     : 2023-09-06 23:17:06
--- @Docs     : 懒加载(魔方|小米提示)
---
require "import"
import "com.androlua.LuaDrawable"
import "android.graphics.RectF"
import "android.widget.TextView"
import "android.widget.FrameLayout"

-- 加载魔方属性
DrawRubik = function()
    return LuaDrawable(function(c, p, d)
        local b = d.bounds

        local width = b.right

        local height = b.bottom

        local scaleState = { 0, width / 24, width / 12, width / 24, width / 12, width / 8, width / 12, width / 8,
            width / 6 }

        local directState = { true, true, true, true, true, true, true, true, true }

        local addNum = width / 6 / 30

        p.setColor(0xff333333)

        return function(c)
            local t = os.clock()

            for k, v in ipairs(scaleState) do
                if directState[k] then
                    directState[k] = scaleState[k] < width / 6

                    scaleState[k] = scaleState[k] + addNum
                else
                    directState[k] = scaleState[k] <= 0

                    scaleState[k] = scaleState[k] - addNum
                end
            end

            c.drawRect(RectF(scaleState[1], scaleState[1], width / 3 - scaleState[1], height / 3 - scaleState[1]), p);

            c.drawRect(RectF(width / 3 + scaleState[2], scaleState[2], width / 3 * 2 - scaleState[2],
                height / 3 - scaleState[2]), p);

            c.drawRect(RectF(width / 3 * 2 + scaleState[3], scaleState[3], width - scaleState[3],
                height / 3 - scaleState[3]), p);

            c.drawRect(RectF(scaleState[4], height / 3 + scaleState[4], width / 3 - scaleState[4],
                height / 3 * 2 - scaleState[4]), p);

            c.drawRect(RectF(width / 3 + scaleState[5], height / 3 + scaleState[5], width / 3 * 2 - scaleState[5],
                height / 3 * 2 - scaleState[5]), p);

            c.drawRect(RectF(width / 3 * 2 + scaleState[6], height / 3 + scaleState[6], width - scaleState[6],
                height / 3 * 2 - scaleState[6]), p);

            c.drawRect(RectF(scaleState[7], height / 3 * 2 + scaleState[7], width / 3 - scaleState[7],
                height - scaleState[7]), p);

            c.drawRect(RectF(width / 3 + scaleState[8], height / 3 * 2 + scaleState[8], width / 3 * 2 - scaleState[8],
                height - scaleState[8]), p);

            c.drawRect(RectF(width / 3 * 2 + scaleState[9], height / 3 * 2 + scaleState[9], width - scaleState[9],
                height - scaleState[9]), p);

            d.invalidateSelf();
        end
    end)
end
