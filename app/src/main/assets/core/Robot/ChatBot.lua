--所以，这个本来是免费源码，被某弟弟1块钱销售
require "import"
import "utils.easy"
LoadImportBase() -- 导入基础模块
BotLayout = import "core.Robot.views.BotLayout"

activity.setTitle("Hook机器人")
--activity.ActionBar.setElevation(0)
activity.ActionBar.setElevation(5)
activity.ActionBar.setBackgroundDrawable(ColorDrawable(0xFF009688))

-- LoadIndexPage(BotLayout)

activity.setContentView(loadlayout(BotLayout))

if Build.VERSION.SDK_INT >= 21 then
  activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS).setStatusBarColor(0xFF009688);
end


SaveCacheData = function(filname, title, str)
  --获取SharedPreferences文件，后面的第一个参数就是文件名，没有会自己创建，有就GetCacheData
  local sp = activity.getSharedPreferences(filname, Context.MODE_PRIVATE)
  --设置编辑
  local sped = sp.edit()
  --设置键值对
  sped.putString(title, str)
  --提交保存数据数
  sped.commit()
  --print("储存成功")
end

GetCacheData = function(filname, title)
  --获取SharedPreferences文件
  local sp = activity.getSharedPreferences(filname, Context.MODE_PRIVATE)
  --打印对应的值
  return sp.getString(title, "")
  --print(sp.getString(title,""))
end

local icon = activity.getLuaDir() .. "/core/Robot/material/"

--载入列表
item = {
  LinearLayout,
  layout_height = "fill",
  id = "pi",
  background = "#F0F0F0",
  layout_width = "fill",
  {
    CircleImageView,
    layout_height = "50dp",
    id = "ji",
    Visibility = 8,
    layout_width = "50dp",
    src = icon .. "jiqi.png",
  },
  {
    LinearLayout,
    id = "piu",
    layout_weight = "1",
    Gravity = 5,
    {
      CardView,
      id = "pin",
      layout_margin = "10dp",
      CardElevation = "2dp",
      radius = "20",
      {
        LinearLayout,
        {
          TextView,
          id = "ti1",
          textSize = "14sp",
          layout_margin = "10dp",
          text = "内容",
        },
      },
    },
  },
  {
    CircleImageView,
    layout_height = "50dp",
    id = "re",
    Visibility = 0,
    layout_width = "50dp",
    src = icon .. "ren.png",
  },
}


data = {}
adp = LuaAdapter(activity, data, item)
table.insert(data,
  {
    ji = { Visibility = View.VISIBLE, },
    re = { Visibility = View.GONE, },
    ti1 = { Text = "主人欢迎回来！", TextColor = int(0xff000000) },
    piu = { Gravity = int(0), },
    pin = { BackgroundColor = int(0xFFFFFFFF) },
  })
list.Adapter = adp
list.onItemClick = function(l, v, p, i)
  activity.getSystemService(Context.CLIPBOARD_SERVICE).setText(v.Tag.ti1.Text)
  Toast.makeText(activity, "复制成功", Toast.LENGTH_SHORT).show()
end

b = nil
--发送消息
RippleHelper(ppsn).RippleColor = 0x59FFFFFF
function ppsn.onClick()
  url = "https://app.qun.qq.com/cgi-bin/api/hookrobot_send?key=" .. GetCacheData("key", "key")

  -- 判断网络信息
  local wl = activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE).getActiveNetworkInfo();
  if wl == nil then
    table.insert(data,
      {
        ji = { Visibility = View.GONE, },
        re = { Visibility = View.VISIBLE, },
        ti1 = { Text = poin.Text, TextColor = int(0xffffffff) },
        piu = { Gravity = int(5), },
        pin = { BackgroundColor = int(0xFF009688) },
      })
    adp.notifyDataSetChanged()
    list.setSelectionFromTop(list.getCount() + 1, 0)
    table.insert(data,
      {
        ji = { Visibility = View.VISIBLE, },
        re = { Visibility = View.GONE, },
        ti1 = { Text = "亲爱的主人你与hook失去了联系…", TextColor = int(0xff000000) },
        piu = { Gravity = int(0), },
        pin = { BackgroundColor = int(0xffffffff) },
      })
    adp.notifyDataSetChanged()
    list.setSelectionFromTop(list.getCount() + 1, 0)
  else -- 联网状态
    table.insert(data,
      {
        ji = { Visibility = View.GONE, },
        re = { Visibility = View.VISIBLE, },
        ti1 = { Text = poin.Text, TextColor = int(0xffffffff) },
        piu = { Gravity = int(5), },
        pin = { BackgroundColor = int(0xFF009688) },
      })
    adp.notifyDataSetChanged()
    list.setSelectionFromTop(list.getCount() + 1, 0)

    import "java.net.URLEncoder" -- Unicode编码

    UpNotify("当前输出:> " .. poin.Text)


    Http.post(url, '{"content": [ {"type":0,"data":"' .. poin.Text .. '"}]}', function(a, b)
      if a == 500 then
        table.insert(data,
          {
            ji = { Visibility = View.VISIBLE, },
            re = { Visibility = View.GONE, },
            ti1 = { Text = "发送成功…", TextColor = int(0xff000000) },
            piu = { Gravity = int(0), },
            pin = { BackgroundColor = int(0xffffffff) },
          })
        adp.notifyDataSetChanged()
        list.setSelectionFromTop(list.getCount() + 1, 0)
      else
        table.insert(data,
          {
            ji = { Visibility = View.VISIBLE, },
            re = { Visibility = View.GONE, },
            ti1 = { Text = "发送失败，请检查key…", TextColor = int(0xff000000) },
            piu = { Gravity = int(0), },
            pin = { BackgroundColor = int(0xffffffff) },
          })
        adp.notifyDataSetChanged()
        list.setSelectionFromTop(list.getCount() + 1, 0)
      end
    end)
  end
  -- 清空文本
  poin.Text = ""
end

--编辑框监听事件(更改按钮控件)
poin.addTextChangedListener {
  onTextChanged = function()
    if #poin.Text == 0 then
      fas.setBackgroundColor(0xFFDADADA)
      ppsn.setEnabled(false)
    else
      fas.setBackgroundColor(0xFF009688)
      ppsn.setEnabled(true)
    end
  end }
ppsn.setEnabled(false)


SendMsg = function()
  table.insert(data,
    {
      ji = { Visibility = View.VISIBLE, },
      re = { Visibility = View.GONE, },
      ti1 = { Text = "发送成功…", TextColor = int(0xff000000) },
      piu = { Gravity = int(0), },
      pin = { BackgroundColor = int(0xffffffff) },
    })
  adp.notifyDataSetChanged()
  list.setSelectionFromTop(list.getCount() + 1, 0)
end


--返回键事件
参数 = 0
function onKeyDown(code, event)
  if string.find(tostring(event), "KEYCODE_BACK") ~= nil then
    if 参数 + 2 > tonumber(os.time()) then
      activity.finish()
    else
      Toast.makeText(activity, "再按一次返回键退出", Toast.LENGTH_SHORT)
          .show()
      参数 = tonumber(os.time())
    end
    return true
  end
end

--[[
--分割字符
t=luajava.astable(String("a,b,c").split(","))
print(table.unpack(t))
]]

MenuTable = { flag = false }

--菜单
function onCreateOptionsMenu(menu)
  -- 输入Key
  menu.add("输入Key").onMenuItemClick = function(a)
    InputLayout = {
      LinearLayout,
      orientation = "vertical",
      Focusable = true,
      FocusableInTouchMode = true,

      {
        EditText,
        layout_width = '80%w',
        layout_height = '6%h',
        layout_gravity = "center",
        textSize = '11dp',
        id = "key",
        hintTextColor = '#69000000',
        textColor = '#FF444444',
        text = GetCacheData("key", "key"),
        Hint = "只能是key~，不能包含其它",
      },

    };
    local c = AlertDialog.Builder(this)
    c.setTitle("输入key")
    c.setView(loadlayout(InputLayout))
    c.setPositiveButton("确定", {
      onClick = function(v)
        SaveCacheData("key", "key", key.Text)
      end
    })
    c.setNegativeButton("取消", nil)
    local c = c.show()
  end
  -- 清空聊天记录
  menu.add("清空聊天记录").onMenuItemClick = function(a)
    adp.clear()                --清空数据
    adp.notifyDataSetChanged() --刷新列表
  end
  -- 使用说明
  menu.add("使用说明").onMenuItemClick = function(a)
    layout = {
      LinearLayout,
      id = "jiomi",
      gravity = "center",
      layout_height = "fill",
      background = "#20000000",
      orientation = "vertical",
      layout_width = "85%w",
      {
        CardView,
        id = "son",
        CardElevation = "5dp",
        layout_height = "280dp",
        layout_width = "85%w",
        background = "#FFFFFF",
        radius = "2dp",
        {
          LinearLayout,
          layout_height = "fill",
          layout_width = "fill",
          orientation = "vertical",
          {
            TextView,
            text = "使用说明",
            gravity = "center",
            textSize = "14sp",
            layout_height = "30dp",
            background = "#FF009688",
            layout_width = "fill",
            textColor = "0xffffffff",
          },
          {
            ScrollView,
            layout_height = "fill",
            layout_width = "fill",
            layout_weight = "1",
            {
              TextView,
              text = [==[
          使用方法：
          将hook机器人拉进群里
          打开消息推送
          获取key
          填入key
          发送消息
          打开QQ查看
          ]==],
              textColor = "#A6000000",
              textSize = "14sp",
            },
          },
          {
            Button,
            text = "我知道了",
            id = "我知道了",
            textSize = "14sp",
            background = "#00000000",
            textColor = "#FF009688",
            layout_width = "fill",
          },
        },
      },
    };


    droid = Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar).setContentView(loadlayout(layout)).show()
    --水波纹
    RippleHelper(我知道了).RippleColor = 0x0C000000
    function 我知道了.onClick()
      droid.dismiss()
    end

    function son.onClick() end

    jiomi.onTouch = function(v, e)
      --触屏按下事件
      if e.getAction() == MotionEvent.ACTION_DOWN then
        droid.dismiss()
      end
    end
  end
  -- 关于程序
  menu.add("关于程序").onMenuItemClick = function(a)
    layout = {
      LinearLayout,
      id = "jiomi",
      gravity = "center",
      layout_height = "fill",
      background = "#20000000",
      orientation = "vertical",
      layout_width = "85%w",
      {
        CardView,
        id = "son",
        CardElevation = "5dp",
        layout_height = "200dp",
        background = "#FFFFFF",
        layout_width = "85%w",
        radius = "2dp",
        {
          LinearLayout,
          layout_height = "fill",
          layout_width = "fill",
          orientation = "vertical",
          {
            TextView,
            text = "更新历史",
            gravity = "center",
            textSize = "14sp",
            layout_height = "30dp",
            background = "#FF009688",
            layout_width = "fill",
            textColor = "0xffffffff",
          },
          {
            ScrollView,
            layout_height = "fill",
            layout_width = "fill",
            layout_weight = "1",
            {
              TextView,
              text = [==[      作者：企鹅君Robin
          界面为小薇机器人界面
          修改成hook机器人发消息']==],
              textColor = "#A6000000",
              textSize = "16sp",
            },
          },
          {
            Button,
            text = "我知道了",
            id = "我知道了",
            textSize = "14sp",
            background = "#00000000",
            textColor = "#FF009688",
            layout_width = "fill",
          },
        },
      },
    };


    droid = Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar).setContentView(loadlayout(layout)).show()
    --水波纹
    RippleHelper(我知道了).RippleColor = 0x0C000000
    function 我知道了.onClick()
      droid.dismiss()
    end

    function son.onClick() end

    jiomi.onTouch = function(v, e)
      --触屏按下事件
      if e.getAction() == MotionEvent.ACTION_DOWN then
        droid.dismiss()
      end
    end
  end

  -- 悬浮窗
  menu.add("悬浮窗").onMenuItemClick = function(a)
    -- dofile(activity.getLuaDir() .. "/core/xf/main.lua"
    if not MenuTable.flag then
      CheckXF()
    else
      UpNotify("你已经启动悬浮窗")
    end
  end
end

CheckXF = function()
  wmManager = activity.getSystemService(Context.WINDOW_SERVICE)         --获取窗口管理器
  HasFocus = false                                                      --是否有焦点
  wmParams = WindowManager.LayoutParams()                               --对象
  if tonumber(Build.VERSION.SDK) >= 26 then
    wmParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY --安卓8以上悬浮窗打开方式
  else
    wmParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT        --安卓8以下的悬浮窗打开方式
  end
  wmParams.format = PixelFormat.RGBA_8888                               --设置背景
  wmParams.flags = WindowManager.LayoutParams().FLAG_NOT_FOCUSABLE      --焦点设置

  wmParams.gravity = Gravity.LEFT| Gravity.TOP                          --重力设置
  wmParams.x = activity.getWidth() / 6
  wmParams.y = activity.getHeight() / 5
  wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT
  wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT

  if Build.VERSION.SDK_INT >= Build.VERSION_CODES.M and ! Settings.canDrawOverlays(this) then
    print("没有悬浮窗权限悬，请打开权限")
    local intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
    intent.setData(Uri.parse("package:" .. activity.getPackageName()));
    activity.startActivityForResult(intent, 100)
    os.exit()
  else
    悬浮球 = loadlayout({
      LinearLayout,
      layout_width = "fill",
      layout_height = "fill",
      {
        CircleImageView,
        src = activity.getLuaDir() .. "/core/xf/res/LuaLogo.png",
        layout_width = "50dp",
        layout_height = "50dp",
        id = "Logo",
      },
    }) --悬浮球
    悬浮窗 = loadlayout(xfc) --悬浮窗
  end

  -- print(WindowManager.LayoutParams())
  -- wmManager.addView(悬浮球, wmParams)
  -- function open.onClick() --开启悬浮窗代码
  -- end

  if MenuTable.flag == false then
    wmManager.addView(悬浮球, wmParams)
    MenuTable.flag = true
  end

  -- wmManager.addView(悬浮球, wmParams)

  function 放大() --放大悬浮窗代码
    wmManager.addView(悬浮窗, wmParams)
    wmManager.removeView(悬浮球)
  end

  function 隐藏() --隐藏悬浮窗代码
    wmManager.removeView(悬浮窗)
    wmManager.addView(悬浮球, wmParams)
  end

  function 退出() --退出悬浮窗代码
    wmManager.removeView(悬浮窗)
    MenuTable.flag = false
  end

  function Close.onClick()
    退出()
  end

  function Min.onClick()
    隐藏()
  end

  function Logo.OnTouchListener(v, event) --这个图标移动代码
    if event.getAction() == MotionEvent.ACTION_DOWN then
      firstX = event.getRawX()
      firstY = event.getRawY()
      wmX = wmParams.x
      wmY = wmParams.y
    elseif event.getAction() == MotionEvent.ACTION_MOVE then
      wmParams.x = wmX + (event.getRawX() - firstX)
      wmParams.y = wmY + (event.getRawY() - firstY)
      wmManager.updateViewLayout(悬浮球, wmParams)
    elseif event.getAction() == MotionEvent.ACTION_UP then
    end
    return false
  end

  function Logo.onClick()
    放大()
  end

  function Windows.OnTouchListener(v, event) --这个图标移动代码
    if event.getAction() == MotionEvent.ACTION_DOWN then
      firstX = event.getRawX()
      firstY = event.getRawY()
      wmX = wmParams.x
      wmY = wmParams.y
    elseif event.getAction() == MotionEvent.ACTION_MOVE then
      wmParams.x = wmX + (event.getRawX() - firstX)
      wmParams.y = wmY + (event.getRawY() - firstY)
      wmManager.updateViewLayout(悬浮窗, wmParams)
    elseif event.getAction() == MotionEvent.ACTION_UP then
    end
    return false
  end

  function CircleButton(view, InsideColor, radiu)
    import "android.graphics.drawable.GradientDrawable"
    drawable = GradientDrawable()
    drawable.setShape(GradientDrawable.RECTANGLE)
    drawable.setColor(InsideColor)
    drawable.setCornerRadii({ radiu, radiu, radiu, radiu, radiu, radiu, radiu, radiu });
    view.setBackgroundDrawable(drawable)
  end

  CircleButton(Execute, 0xFFFF9400, 45)
  CircleButton(Close, 0xffff461f, 70)
  CircleButton(Min, 0xff50616d, 20)


  function Execute.onClick()
    --shell命令的方法
    -- MD提示("加油哦!你是最棒的!", 0xFF2196F3, 0xFFFFFFFF, 4, 10)
    UpNotify("加油哦!你是最棒的!")
  end
end
