require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "utils.easy"
LoadImportBase()
import "core.xf.xfq"
import "core.xf.xfq"
-- droid = Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar).setContentView(loadlayout(layout)).show()
-- --水波纹
-- RippleHelper(我知道了).RippleColor = 0x0C000000
-- function 我知道了.onClick()
--   droid.dismiss()
-- end

-- function son.onClick() end

-- jiomi.onTouch = function(v, e)
--   --触屏按下事件
--   if e.getAction() == MotionEvent.ACTION_DOWN then
--     droid.dismiss()
--   end
-- end
local xfc = import "core.xf.xfc"
local xfq = import "core.xf.xfq"


import "android.graphics.PixelFormat"
import "android.content.Context"
import "android.provider.Settings"
import "android.animation.ObjectAnimator"
import "android.animation.ArgbEvaluator"


activity.setContentView(loadlayout({
  LinearLayout,
  orientation = "vertical",
  layout_width = "fill",
  layout_height = "fill",
  backgroundColor = "0xFFFF5300",
  gravity = "center",
  {
    Button,
    id = "open",
    text = "打开悬浮窗",
  },
}))

wmManager = activity.getSystemService(Context.WINDOW_SERVICE)         --获取窗口管理器
HasFocus = false                                                      --是否有焦点
wmParams = WindowManager.LayoutParams()                               --对象
if tonumber(Build.VERSION.SDK) >= 26 then
  wmParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY --安卓8以上悬浮窗打开方式
else
  wmParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT        --安卓8以下的悬浮窗打开方式
end
wmParams.format = PixelFormat.RGBA_8888                               --设置背景
wmParams.flags = WindowManager.LayoutParams().FLAG_NOT_FOCUSABLE      --焦点设置

wmParams.gravity = Gravity.LEFT| Gravity.TOP                          --重力设置
wmParams.x = activity.getWidth() / 6
wmParams.y = activity.getHeight() / 5
wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT
wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT




if Build.VERSION.SDK_INT >= Build.VERSION_CODES.M and ! Settings.canDrawOverlays(this) then
  print("没有悬浮窗权限悬，请打开权限")
  local intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
  intent.setData(Uri.parse("package:" .. activity.getPackageName()));
  activity.startActivityForResult(intent, 100)
  os.exit()
else
  悬浮球 = loadlayout({
  LinearLayout;
  layout_width="fill";
  layout_height="fill";
  {
    CircleImageView;
    src=activity.getLuaDir().."/core/xf/res/LuaLogo.png";
    layout_width="50dp";
    layout_height="50dp";
    id="Logo";
  };
}) --悬浮球
  悬浮窗 = loadlayout(xfc) --悬浮窗
end

--print(WindowManager.LayoutParams())
-- wmManager.addView(悬浮球, wmParams)
function open.onClick() --开启悬浮窗代码
  if 悬浮球是否打开 == nil then
    wmManager.addView(悬浮球, wmParams)
    悬浮球是否打开 = true
  else
    UpNotify("你已经启动悬浮窗")
  end
end


function 放大() --放大悬浮窗代码
  wmManager.addView(悬浮窗, wmParams)
  wmManager.removeView(悬浮球)
end

function 隐藏() --隐藏悬浮窗代码
  wmManager.removeView(悬浮窗)
  wmManager.addView(悬浮球, wmParams)
end

function 退出() --退出悬浮窗代码
  wmManager.removeView(悬浮窗)
  悬浮球是否打开 = nil
end

function Close.onClick()
  退出()
end

function Min.onClick()
  隐藏()
end

function Logo.OnTouchListener(v, event) --这个图标移动代码
  if event.getAction() == MotionEvent.ACTION_DOWN then
    firstX = event.getRawX()
    firstY = event.getRawY()
    wmX = wmParams.x
    wmY = wmParams.y
  elseif event.getAction() == MotionEvent.ACTION_MOVE then
    wmParams.x = wmX + (event.getRawX() - firstX)
    wmParams.y = wmY + (event.getRawY() - firstY)
    wmManager.updateViewLayout(悬浮球, wmParams)
  elseif event.getAction() == MotionEvent.ACTION_UP then
  end
  return false
end

function Logo.onClick()
  放大()
end

function Windows.OnTouchListener(v, event) --这个图标移动代码
  if event.getAction() == MotionEvent.ACTION_DOWN then
    firstX = event.getRawX()
    firstY = event.getRawY()
    wmX = wmParams.x
    wmY = wmParams.y
  elseif event.getAction() == MotionEvent.ACTION_MOVE then
    wmParams.x = wmX + (event.getRawX() - firstX)
    wmParams.y = wmY + (event.getRawY() - firstY)
    wmManager.updateViewLayout(悬浮窗, wmParams)
  elseif event.getAction() == MotionEvent.ACTION_UP then
  end
  return false
end

function CircleButton(view, InsideColor, radiu)
  import "android.graphics.drawable.GradientDrawable"
  drawable = GradientDrawable()
  drawable.setShape(GradientDrawable.RECTANGLE)
  drawable.setColor(InsideColor)
  drawable.setCornerRadii({ radiu, radiu, radiu, radiu, radiu, radiu, radiu, radiu });
  view.setBackgroundDrawable(drawable)
end

-- CircleButton(open, 0xFFFF9400, 30)


function Execute.onClick()
  --shell命令的方法
  -- MD提示("加油哦!你是最棒的!", 0xFF2196F3, 0xFFFFFFFF, 4, 10)
  UpNotify("加油哦!你是最棒的!")
end

function Clear.onClick()
  Editor.setText("");
end
