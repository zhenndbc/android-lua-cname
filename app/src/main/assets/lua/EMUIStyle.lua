pcall(function()androidhwext={R=luajava.bindClass("androidhwext.R")} end)
--使用方法：
--[[
import "EMUIStyle"
if androidhwext then
  pcall(function()--必须pcall一下，因为使用hwext的系统不只有EMUI
    activity.setTheme(androidhwext.R.style.Theme_Emui)
  end)
end
]]
