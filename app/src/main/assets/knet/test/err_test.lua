---
--- @File : err_test
--- @Time : 2023-09-03 23:33:56
--- @Info : 错误信息和回跟踪(Tracebacks)
--- @Docs : Lua进阶 -> 错误处理 pcall - error
---

-- 通常情况下，我们使用字符串来描述遇到的错误信息。
local _, err = pcall(function() return "Hello" end)
print(err)

-- 通过传递给 error 更加细致地获取 错误信息
_, err = pcall(function() error("My error") end)
print(err)

-- 通过设置 error 等级信息 获取更加细致的数据 (自己的代码之外的级别)
local thanErr = function(str)
    if type(str) ~= "string" then
        error("string expected", 2)
    end
end
print(thanErr({ x = 2 }))

-- 期望有一个完整的显示导致错误发生的调用栈的 tracebacks
-- 当 pcall 返回错误信息的时候他已经释放了保存错误发生情况的栈的信息。
-- debug.traceback() 获取更加细致的数据
print(debug.traceback())
print(debug.debug())
