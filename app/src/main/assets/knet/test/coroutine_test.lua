---
--- @File Name: coroutine_test
--- @Time     : coroutine_test
--- @Info     : 正在运行的协同程序只有在明确的被要求挂起的时候才会被挂起
--- @Docs     : 协同程序(coroutine)
---

-- 创建协程函数
local co = coroutine.create(function(a, b)
    print("Hello world")
    print(a, b)
    return a + b
end)

-- 获取数据
local flag, info = coroutine.resume(co, 1, 2)
print(flag, info)

-- 获取状态
print(coroutine.status(co))


co = coroutine.create(function()
    -- task
    local task = function(v)
        print(v)
    end

    for _, value in ipairs({ 1, 2, 3, 4, 5, 6 }) do
        task(value)
        coroutine.yield()
    end
end)

local checkTaskStatus = function(f)
    repeat
        _ = coroutine.resume(f)
    until not _
end

checkTaskStatus(co)
