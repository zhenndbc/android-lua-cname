require "lua.import"
import "knet.fetch.promise"

local SchoolLogin = {
  username = "",
  password = "",
  base_url = "http://61.186.94.104:8090",
  img_url = "http://61.186.94.104:8090/verifycode.servlet",
  str_url = "http://61.186.94.104:8090/Logon.do?method=logon&flag=sess",
  login_url = "http://61.186.94.104:8090/Logon.do?method=logon",
  logout_url = "http://61.186.94.104:8090/jsxsd/xk/LoginToXk",
  status = 0
}

function SchoolLogin:new(user_info)
  local o = {}
  setmetatable(o, self)
  self.__index = self
  o.username = user_info.username
  o.password = user_info.password
  o:keep_alive()
  return o
end

function SchoolLogin:keep_alive()
  local headers = {
    ['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
  }
  promise:start(function(next)
    Http.get(self.base_url, nil, "utf8", headers, function(code, body, cookie, header)
      if code ~= 200 then
        next.reject("访问失败", code)
       else
        next.resolve(code, cookie)
      end
    end)
  end):next(function(next, code, cookie)
    print("请求完成", cookie)
    self.status = code

    -- 在这里执行需要在状态设置后执行的操作
    self:doSomethingAfterStatusUpdated()
  end):catch_err(function(error, code)
    self.status = code
    print("Error:", error)
  end)
end

-- 添加一个函数来执行在状态设置后需要执行的操作
function SchoolLogin:doSomethingAfterStatusUpdated()
  -- 这里执行你需要的操作，可以访问 self.status 了
  print("现在的状态是:", self.status)
end


function SchoolLogin:_get_verify_code()
  local headers = {
    ['Accept'] = 'image/webp,image/apng,image/*,*/*;q=0.8',
    ['Accept-Encoding'] = 'gzip, deflate',
    ['Accept-Language'] = 'zh-CN,zh;q=0.9',
    ['Connection'] = 'keep-alive',
    ['Host'] = '61.186.94.104:8090',
    ['Referer'] = 'http://61.186.94.104:8090',
    ['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
  }
  local safe_resp = Http.get(self.img_url, { headers = headers })
  -- 验证码解析
  print(safe_resp)
  return text
end

function SchoolLogin:_get_encode()
  local headers = {
    ['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    ['Accept-Encoding'] = 'gzip, deflate',
    ['Accept-Language'] = 'zh-CN,zh;q=0.9',
    ['Cache-Control'] = 'max-age=0',
    ['Connection'] = 'keep-alive',
    ['Host'] = '61.186.94.104:8090',
    ['Upgrade-Insecure-Requests'] = '1',
    ['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
  }
  local data_str = http.get(self.str_url, { headers = headers }):text()
  local encode = ""
  local scode = string.match(data_str, "(.-)#")
  local sxh = string.match(data_str, "#(.+)")
  local code = self.username .. "%%%" .. self.password
  for i = 1, #code do
    if i < 20 then
      encode = encode .. string.sub(code, i, i) .. string.sub(scode, 1, tonumber(string.sub(sxh, i, i)))
      scode = string.sub(scode, tonumber(string.sub(sxh, i, i)) + 1)
     else
      encode = encode .. string.sub(code, i)
    end
  end
  encode = string.sub(encode, 1, -15)
  return encode
end

function SchoolLogin:_login()
  local verify_code = self:_get_verify_code()
  local encoded = self:_get_encode()
  local data = {
    userAccount = self.username,
    user__Password = "",
    RANDOMCODE = verify_code,
    encoded = encoded
  }
  local headers = {
    ['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    ['Accept-Encoding'] = 'gzip, deflate',
    ['Accept-Language'] = 'zh-CN,zh;q=0.9',
    ['Cache-Control'] = 'max-age=0',
    ['Connection'] = 'keep-alive',
    ['Content-Length'] = '101',
    ['Content-Type'] = 'application/x-www-form-urlencoded',
    ['Host'] = '61.186.94.104:8090',
    ['Origin'] = 'http://61.186.94.104:8090',
    ['Referer'] = 'http://61.186.94.104:8090/',
    ['Upgrade-Insecure-Requests'] = '1',
    ['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
  }
  local resp = http.post(self.login_url, { headers = headers, data = data })
  resp:raise_for_status()
  return resp
end

function SchoolLogin:exit_login()
  local headers = {
    ['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    ['Accept-Language'] = 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    ['Origin'] = 'http://61.186.94.104:8090',
    ['Proxy-Connection'] = 'keep-alive',
    ['Referer'] = 'http://61.186.94.104:8090/jsxsd/',
    ['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
    ['Host'] = '61.186.94.104:8090',
    ['Connection'] = 'keep-alive'
  }
  local resp = http.get(self.logout_url, { headers = headers, data = { method = 'exit' } })
  assert(resp.status_code == 200, "退出失败")
end


return SchoolLogin